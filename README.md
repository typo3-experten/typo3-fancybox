# TYPO3 Extension `fancyBox`

[![Donate](https://img.shields.io/badge/Donate-PayPal-green.svg?style=for-the-badge)](https://paypal.me/pmlavitto)
[![Latest Stable Version](https://img.shields.io/packagist/v/lavitto/typo3-fancybox?style=for-the-badge)](https://packagist.org/packages/lavitto/typo3-fancybox)
[![TYPO3](https://img.shields.io/badge/TYPO3-fancybox-%23f49700?style=for-the-badge)](https://extensions.typo3.org/extension/fancybox/)
[![License](https://img.shields.io/packagist/l/lavitto/typo3-fancybox?style=for-the-badge)](https://packagist.org/packages/lavitto/typo3-fancybox)

> This extension adds a fancybox to your TYPO3 website.

- **Demo**: [www.lavitto.ch/typo3-ext-fancybox](https://www.lavitto.ch/typo3-ext-fancybox)
- **Gitlab Repository**: [gitlab.com/lavitto/typo3-fancybox](https://gitlab.com/lavitto/typo3-fancybox)
- **TYPO3 Extension Repository**: [extensions.typo3.org/extension/fancybox](https://extensions.typo3.org/extension/fancybox/)
- **Found an issue?**: [gitlab.com/lavitto/typo3-fancybox/issues](https://gitlab.com/lavitto/typo3-fancybox/issues)

## 1. Introduction

### Features

- Based on extbase & fluid
- Simple and fast installation
- No configuration needed
- Fully responsive

## 2. Installation

### Installation using Composer

The recommended way to install the extension is by using [Composer](https://getcomposer.org/). In your Composer based 
TYPO3 project root, just do `composer req lavitto/typo3-fancybox`.

### Installation from TYPO3 Extension Repository (TER)

Download and install the extension `fancybox` with the extension manager module.

## 3. Setup

1)  Simple include the static TypoScript of the extension.

## 4. Configuration

### Constants

This default properties can be changed by **Constant Editor**:

| Property           | Description                                    | Type      | Default value   |
| ------------------ | ---------------------------------------------- | --------- | --------------- |
| enableJquery       | Includes jQuery to the page                    | boolean   | false           |

## 6. fancybox License

**Please note the license requirements of the fancybox, which is used by this extension!**

fancybox is licensed under the GPLv3 license for all open source applications but a commercial license is required for all commercial applications (including sites, themes and apps you plan to sell).

More information about the fancybox license can be found here:
https://fancyapps.com/fancybox/3/#license

## 7. Contribute

Please create an issue at https://gitlab.com/lavitto/typo3-fancybox/issues.

**Please use GitLab only for bug-reporting or feature-requests. For support use the TYPO3 community channels or contact us by email.**

## 7. Support

If you need private or personal support, contact us by email on [info@lavitto.ch](mailto:info@lavitto.ch). 

**Be aware that this support might not be free!**
